import java.io.BufferedReader;
import java.io.*; 
import java.io.File;
import java.util.Arrays;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;
 
// Class for storing the entire history of drivers
public class TravelLog {
    public String path;
    private Map<String, DriverRecord> driverHistoryLog = new HashMap<String, DriverRecord>();

    public TravelLog(String path) {
        this.path = path;
    }

    // function call from Main to compute 
    public void calculateOutput() {
        File dir = new File(this.path);
        File[] files = dir.listFiles();
        
        try{
            for(File file : files) {
                if (file.getName().toLowerCase().endsWith((".txt"))) {
                    trackHistory(file.getPath());
                }
            }
        }
        catch(NullPointerException e) {
            if (this.path.toLowerCase().endsWith((".txt"))) {
                trackHistory(this.path);
            }
        }
    }

    // displays output in "output.txt" and on console/terminal
    public String showOutput() {
        try {
            PrintWriter writer = new PrintWriter("output.txt", "UTF-8");
            Set<Entry<String, DriverRecord>> set = driverHistoryLog.entrySet();
            List<Entry<String, DriverRecord>> list = new ArrayList<Entry<String, DriverRecord>>(set);
            Collections.sort(list, new Comparator<Map.Entry<String, DriverRecord>>() {
                public int compare(Map.Entry<String, DriverRecord> o1, Map.Entry<String, DriverRecord> o2 ){
                    return ((Integer) o2.getValue().getDistance()).compareTo(o1.getValue().getDistance());
                }
            });
            for(Map.Entry<String, DriverRecord> entry:list){
                String printOutput;
                if(entry.getValue().getDistance() == 0) printOutput = entry.getValue().name + ": " + entry.getValue().getDistance() + " miles";
                else printOutput = entry.getValue().name + ": " + entry.getValue().getDistance() + " miles @ " + entry.getValue().getSpeed() + " mph";
                System.out.println(printOutput);
                writer.println(printOutput);
            }
            writer.close();
        }
        catch(Exception e) {
            return "";
        }
        return "output.txt";
    }

    // function the determine if file exist or not
    public void trackHistory(String filePath) {
        File file = new File(filePath);
        try {
            FileReader fr = new FileReader(file);
            BufferedReader br = new BufferedReader(fr);
            String line;
            while ((line = br.readLine()) != null) {
                fillHistoryLog(line);
            }
            br.close();
        }
        catch(FileNotFoundException e) {
            System.out.println("File missing");
        }
        catch(IOException e) {
            System.out.println("String in file is not proper");
        }       
    }

    // function to determine update or create driver
    private void fillHistoryLog(String line) {
        String[] content = line.split("\\s+");
        if(content[0].equalsIgnoreCase("driver")) {
            addNewDriverEntry(Arrays.copyOfRange(content, 1, content.length)); 
        }
        else if(content[0].equalsIgnoreCase("trip")) {
            updateDriverHistory(Arrays.copyOfRange(content, 1, content.length));
        }
    }

    // function to add new driver to the list
    private void addNewDriverEntry(String[] content) {
        DriverRecord driver = new DriverRecord(content[0]);
        driverHistoryLog.put(content[0].toLowerCase(),driver);
    }

    // function to update driver's stat
    private void updateDriverHistory(String[] content) {
        if(content.length != 4) {
            System.out.println("Invalid input");
        }
        if(driverHistoryLog.containsKey(content[0].toLowerCase())) {
            DriverRecord driver = driverHistoryLog.get(content[0].toLowerCase());
            driver.updateValues(Arrays.copyOfRange(content, 1, content.length));
        }
        else{
            System.out.println("Driver '"+ content[0] +"' Not Found");
        }
    } 
}