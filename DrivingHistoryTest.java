import java.io.BufferedReader;
import java.io.*; 
import java.io.File;

// Class to test
class DrivingHistoryTest {
    public String pathInputFile;
    public String pathOutputFile;

    public DrivingHistoryTest(String input, String output) {
        this.pathInputFile = input;
        this.pathOutputFile = output;
    }


    // test function called from Main to test the whole output
    public boolean testOutput() {
        if(logAssert(this.pathInputFile, this.pathOutputFile)){
            return true;
        }
        else return false;
    }

    // function that takes the input and output to compare with projected output
    private boolean logAssert(String inputPath, String outputPath) {
        TravelLog log = new TravelLog(inputPath); 
        
        if(fileCompare(outputPath, log.showOutput())){
            return true;
        }
        else return false;

    }

    // function for comparing the output in the file
    private boolean fileCompare(String outputPath, String projectedOutputPath) {
        boolean areEqual = true;        
        try{
            BufferedReader reader1 = new BufferedReader(new FileReader(outputPath));        
            BufferedReader reader2 = new BufferedReader(new FileReader(projectedOutputPath));
            String line1 = reader1.readLine();
            String line2 = reader2.readLine();
            
            int lineNum = 1;
            while (line1 != null || line2 != null){
                if(line1 == null || line2 == null){
                    areEqual = false;      
                    break;
                }
                else if(! line1.equalsIgnoreCase(line2)){
                    areEqual = false;                 
                    break;
                }           
                line1 = reader1.readLine();          
                line2 = reader2.readLine();             
                lineNum++;
            }

            reader1.close();         
            reader2.close();

            if(areEqual) {
                return true;
            }
            else{
                return false;
            }        
        }
        catch(IOException e){
            return false;
        }      
    }
}