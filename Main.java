import java.util.Scanner;

// Entry point
public class Main {
    public static void main(String[] args) {
        System.out.println("Select a number (1:Run / 2:Test)");
        Scanner sc = new Scanner(System.in);
        int op = sc.nextInt();
        switch(op) {
            // testing switch case
            case 2:           
            System.out.println("Please insert the path of test input file:");
            String inputTest = sc.next();
            System.out.println("Please insert the path of test output file:");
            String outputTest = sc.next();

            if(inputTest == "") {
                inputTest = "testData/input.txt";
            }
            if(outputTest == "") {
                outputTest = "testData/output.txt";
            }
            DrivingHistoryTest test = new DrivingHistoryTest(inputTest, outputTest);
            if(test.testOutput()) {
                System.out.println("Test Case Passed");
            }
            else {
                System.out.println("Test Case Failed");
            }
            break;

            // run switch case
            case 1:
            System.out.println("Please insert the path of directory or file:");
            String path = sc.next();
            //String path = "";
            if(path == "") {
                path = "data/";
            }
            TravelLog log = new TravelLog(path); 
            log.calculateOutput();
            log.showOutput();
            break;

            default:
            System.out.println("Wrong Input");
            break;
        }
        sc.close();
    }
}