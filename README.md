Problem Statement:

Let's write some code to track driving history for people.

The code will process an input file. You can either choose to accept the input via stdin (e.g. if you're using Ruby cat input.txt | ruby yourcode.rb), or as a file name given on the command line (e.g. ruby yourcode.rb input.txt). You can use any programming language that you want. Please choose a language that allows you to best demonstrate your programming ability.

Each line in the input file will start with a command. There are two possible commands.

The first command is Driver, which will register a new Driver in the app. Example:

Driver Dan

The second command is Trip, which will record a trip attributed to a driver. The line will be space delimited with the following fields: the command (Trip), driver name, start time, stop time, miles driven. Times will be given in the format of hours:minutes. We'll use a 24-hour clock and will assume that drivers never drive past midnight (the start time will always be before the end time). Example:

Trip Dan 07:15 07:45 17.3

Discard any trips that average a speed of less than 5 mph or greater than 100 mph.

Generate a report containing each driver with total miles driven and average speed. Sort the output by most miles driven to least. Round miles and miles per hour to the nearest  integer.

Example input:<br/>

Driver Dan<br/>
Driver Alex<br/>
Driver Bob<br/>
Trip Dan 07:15 07:45 17.3<br/>
Trip Dan 06:12 06:32 21.8<br/>
Trip Alex 12:01 13:16 42.0<br/>

Expected output:<br/>

Alex: 42 miles @ 34 mph<br/>
Dan: 39 miles @ 47 mph<br/>
Bob: 0 miles<br/>

--------------------------------

Solution:

Steps to Run:

1) Compile: javac *.java and Run: java Main<br/>
2) Select option to test(2) or run(1)<br/>
3) If(1) : input the path of directory(to calculate from multiple) or file(to calculate only from one)<br/>
4) If(2) : input the path of input file and output file to check if the output matches<br/>
5) Output is stored in "output.txt" and it can be shown on the console/terminal<br/>

Thought Process:

1) I wanted to store each driver record thus I created a model/class for storing the amount of distance he travelled and time spent<br/>
2) I created TravelLog class so that it can be called with path and it can be easily testable<br/>
3) TravelLog stores the history of the drivers in a Map<String, DriveRecord> thus making it easy to retrieve and update<br/>
4) Thus, different logs can be created depending upon their path<br/>

