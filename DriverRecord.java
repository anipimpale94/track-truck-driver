import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

// Class for storing driver's complete info
public class DriverRecord {
    public String name;
    private int tripTime = 0;
    private float distanceTravelled = 0;

    public DriverRecord(String name) {
        this.name = name;    
    }

    // fetch distance of each driver
    public int getDistance() { 
        return Math.round(this.distanceTravelled);
    }

    // fetch speed of each driver
    public int getSpeed() {
        return calculateSpeed(this.distanceTravelled, this.tripTime);     
    }

    // Update driver's stats
    public void updateValues(String[] content) {
        DateFormat df = new SimpleDateFormat("HH:mm");
        try {
            Date start = df.parse(content[0]);
            Date end = df.parse(content[1]);

            int timeDiff = (int)(end.getTime() - start.getTime())/60000;
            if(timeDiff <= 0) return;
            float distance = Float.parseFloat(content[2]);
            int speed = calculateSpeed(distance, timeDiff);
            if(speed < 5 && speed > 100) {
                return;
            }
            this.distanceTravelled += distance;
            this.tripTime += timeDiff;
        }
        catch(Exception e) {
            return;
        }   
    }
    
    // function to calculate speed of driver
    private int calculateSpeed(float distance, int time) {
        float speed = distance / time * 60;
        return Math.round(speed);
    }
}